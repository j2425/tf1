#output "instances" {
#  description = "instance_id, instance_public_ip, instance_name"
#  value = {
#    instance_id = "${aws_instance.app_server.*.id}"
#    public_ip  = "${aws_instance.app_server.*.public_ip}"
#    instance_name = "${aws_instance.app_server.*.public_dns}"
#  }
#}

output "instances" {
  description = "instance_id, instance_public_ip, instance_name"
  value = [    for idx, inst_id in aws_instance.app_server[*].id :
    {
      instance_id = inst_id
      instance_name = aws_instance.app_server[idx].public_dns
      public_ip = aws_instance.app_server[idx].public_ip
    }
  ]
}

output "vpc_id" {
  description = "ID of the instances VPC"
  value = data.aws_vpc.default.id
}
