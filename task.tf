data "aws_vpc" "default" {
  default = true
}

variable "node_count" {
  default = "3"
  }

variable private_key_path {
  description = "private key for connect"
}

variable "tf53_traffic_rules" {
  type = list(object({
    from_port = number
    to_port = number
    protocol = string
    cidr_blocks = list(string)
  }))
  default = [
    {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port = 80 
      to_port = 80
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port = 443
      to_port = 443
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port = 10000
      to_port = 20000
      protocol = "udp"
      cidr_blocks = ["10.0.0.23/32"]
    }
  ]
}
  
resource "aws_security_group" "tf53_security_group" {
  name = "tf53_security_group"
  description = "Allow inbound traffic and restrict 443 only for certain"

  dynamic "ingress" {
    for_each = var.tf53_traffic_rules
    content {
     from_port = ingress.value.from_port
     to_port = ingress.value.to_port
     protocol = ingress.value.protocol
     cidr_blocks = ingress.value.cidr_blocks
    }
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "tf53_sg"
  }
}

resource "aws_instance" "app_server" {
  ami           = "ami-00ad2436e75246bba"
  instance_type = "t2.micro"
  iam_instance_profile = "DemoRoleforEC2"
  key_name = "tutorial_key"
  security_groups = ["tf53_security_group"]
  root_block_device {
    volume_size = "20"
  }

  count = "${var.node_count}"

  tags = {
    Name = "node${count.index + 1}"
  }
  
  provisioner "local-exec" {
    command = "echo '${self.public_dns} ${self.public_ip}' >> hosts.list"
  }
  
  connection {
    type = "ssh"
    user = "ec2-user"
    private_key = "${file(var.private_key_path)}"
    timeout = "4m"
    host = self.public_ip
  }
  provisioner "remote-exec" {
    inline = [
      "sudo yum install -y nginx",
      "sudo chmod g+w /usr/share/nginx/html/index.html",
      "sudo chgrp ec2-user /usr/share/nginx/html/index.html",
      "sudo echo 'Juneway ${self.tags.Name} ${self.public_ip}' > /usr/share/nginx/html/index.html",
      "sudo nginx",
    ]  
  }
}
